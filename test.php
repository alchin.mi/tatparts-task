<?php

class OrderService{
private $db;
public function __construct($db){
    $this->db = $db;
}
public function processOrders($orders){
    $processedOrders = [];

    foreach ($orders as $order) {
        $customer = $this->db->query("SELECT * FROM customers WHERE id = " . $order['customer_id']);
        $product = $this->db->query("SELECT * FROM products WHERE id = " . $order['product_id']);

        $totalAmount = ($product['price'] * 1.20) * $order['quantity'];

        $discount = $this->calculateDiscount($customer, $totalAmount);

        $finalAmount = $totalAmount - $discount;

        $this->db->query("UPDATE orders SET amount = $finalAmount WHERE id = " . $order['id']);

        $processedOrders[] = [
            'order_id' => $order['id'],
            'final_amount' => $finalAmount
        ];
    }
    return $processedOrders;
}
private function calculateDiscount($customer, $amount){
    // Логика для определения скидки в зависимости от типа клиента
    $markupGroup = $this->db->query("SELECT markup_group FROM customer_groups WHERE id = " . $customer['group_id']);

    if (!$markupGroup) {
        return 0;
    }

    $markup = (float)$markupGroup['markup'];

    $discount = $amount * ($markup / 100);

    return $discount;
}
}